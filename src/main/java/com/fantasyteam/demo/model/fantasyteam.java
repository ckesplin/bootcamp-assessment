package com.fantasyteam.demo.model;

import javax.persistence.*;


//@Entity -  marks a class to be put in a table.  Spring core capability.   javax.persistence.Entity library.
@Entity
public class fantasyteam {
    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    @Column
    private String name;
    @Column
    private int number;
    @ManyToOne
    @JoinColumn(name="positions", referencedColumnName = "ID")
    private positions position;
    @ManyToOne
    @JoinColumn(name="teams", referencedColumnName = "ID")
    private teams teams;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public com.fantasyteam.demo.model.teams getTeams() {return teams;}

    public void setTeams(com.fantasyteam.demo.model.teams teams) {this.teams = teams;}


    public positions getPosition() {return position;}

    public void setPosition(positions position) {this.position = position;}
}

