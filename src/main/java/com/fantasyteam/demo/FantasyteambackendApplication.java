package com.fantasyteam.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FantasyteambackendApplication {

    public static void main(String[] args) {
        SpringApplication.run(FantasyteambackendApplication.class, args);
    }

}
