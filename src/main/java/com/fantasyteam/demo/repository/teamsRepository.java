package com.fantasyteam.demo.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import com.fantasyteam.demo.model.teams;

@Repository
public interface teamsRepository extends CrudRepository<teams, Long> {

}
