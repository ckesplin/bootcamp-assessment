package com.fantasyteam.demo.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import com.fantasyteam.demo.model.fantasyteam;

@Repository
// CrudRepository<[data you are targeting - the model name], [type of variable of the primary key of the database]>
public interface fantasyteamRepositorty extends CrudRepository<fantasyteam, Long> {
}
