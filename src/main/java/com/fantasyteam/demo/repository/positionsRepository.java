package com.fantasyteam.demo.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import com.fantasyteam.demo.model.positions;

@Repository
public interface positionsRepository extends CrudRepository<positions, Long> {
}
