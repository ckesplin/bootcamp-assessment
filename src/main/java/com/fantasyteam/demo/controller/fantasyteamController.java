package com.fantasyteam.demo.controller;

import com.fantasyteam.demo.model.*;
import com.fantasyteam.demo.repository.*;
import org.springframework.web.bind.annotation.*;

import javax.swing.text.Position;
import java.util.List;

@RestController
@CrossOrigin
public class fantasyteamController {
    private fantasyteamRepositorty repo;
    private teamsRepository teamRepo;
    private positionsRepository positionRepo;


    // constructor for the controller
    public fantasyteamController(fantasyteamRepositorty fantasyrepo,
                                 teamsRepository teamRepo,
                                 positionsRepository positionRepo) {
        this.repo = fantasyrepo;
        this.teamRepo = teamRepo;
        this.positionRepo = positionRepo;
    }

    @GetMapping("/team")
    public List<fantasyteam> nameList() {
        // this can be written return (List<fantasyteam>) repo.findAll();  -- findAll() is a spring method
        List<fantasyteam> list = (List<fantasyteam>) repo.findAll();
//        System.out.println(list.size());
        return list;
    }

    @GetMapping("/teamsList")
    public List<teams> teamList() {
        List<teams> list = (List<teams>) teamRepo.findAll();
        return list;
    }

    @GetMapping("/positionList")
    public List<positions> positionList() {
        List<positions> list = (List<positions>) positionRepo.findAll();
        return list;
    }

    @PostMapping("/newplayer")
    public void createPlayer(@RequestBody fantasyteam player){
        repo.save(player);
    }

    @DeleteMapping("/removeplayer/{playerId}")
    public void removePlayer(@PathVariable long playerId){
        repo.deleteById(playerId);
    }
}

